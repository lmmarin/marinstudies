package Lists.ExercicioFxacao;

/**
 * @author lmarin
 * @version : $<br/>
 * : $
 * @since 12/11/2019 12:27
 */
public class People {
    int id;
    String name;
    private double salary;


    public People(int idPeople, String n, double s){
        this.id = idPeople;
        this.name = n;
        this.salary = s;
    }

    public People(){
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(final double salary) {
        this.salary = salary;
    }

    public String toString(){
        return "ID: "+id+
                "\nNome: "+name+
                "\nSalario: "+salary;
    }

}
