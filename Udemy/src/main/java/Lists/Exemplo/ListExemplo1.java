package Lists.Exemplo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lmarin
 * @version : $<br/>
 * : $
 * @since 11/11/2019 18:04
 */
public class ListExemplo1 {

    public static void main(String[] args) {

        List<String> list = new ArrayList<String>();
        list.add("Lucas");
        list.add("Maria");
        list.add("Murillo");
        list.add("Joao");
        list.add("Marin");
        list.add("Loucura");
        //Adicionado o Inteiro antes do nome do elemento ele substitui o outro que estiver na posi��o. no caso a Maria pelo exemplo
        list.add(2,"exemplo");

        System.out.println("Tamanho da lista: "+list.size());
        //remove tudo o que tem na posi��o 1 da lista.
        list.remove(1);
        //Remove Tudo que tem Marin,
        list.remove("Marin");

        for(String obj : list){
            System.out.println(obj);
        }

        System.out.println("------------------------------------");
       list.removeIf(obj -> obj.charAt(0) == 'M');
        for(String obj : list){
            System.out.println(obj);
        }
        System.out.println("-----------------------------------");
        System.out.println("Index of Lucas: "+ list.indexOf("Lucas"));

    }
}
