package metodosEstaticosAula.exemplo2;

// CLASSE QUE VAI OFERECER AS FUNCOES E METODOS DELA PARA SEREM USADOS, EXEMPLO DISSO SERIA O MATH.SQRT()

public class classcircuferenceexemple {
    private static final double P_I = 3.1415;

    public double circuference(double x){
        return 2.0 * P_I * x;
    }

    public double volume(double x){
        return 4.0 * P_I * x * x * x / 3.0;
    }

}
