package metodosEstaticosAula.exemplo2;

import java.util.Locale;
import java.util.Scanner;

        //CLASSE PRINCIPAL QUE VAI MOSTRAR COMO SE UTILIZA OS METODOS ATRAVES DE OUTRA CLASSE.

public class classeesticaexemplo2 {

        public static void main(String[] args) {

            classcircuferenceexemple circuference = new classcircuferenceexemple();

            double a,b,c;

            Locale.setDefault(Locale.US);
            Scanner scanner = new Scanner(System.in);

            System.out.println("Number 1: ");
            a = scanner.nextDouble();

            b = circuference.circuference(a);
            System.out.println("Circuference: "+b);
            c = circuference.volume(a);
            System.out.println("Volum: "+c);
        }

}
