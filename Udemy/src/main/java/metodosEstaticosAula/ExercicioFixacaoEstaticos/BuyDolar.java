package metodosEstaticosAula.ExercicioFixacaoEstaticos;

import java.util.Locale;
import java.util.Scanner;

public class BuyDolar {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);

        double dolar;

        System.out.println("Quanto de Dolar quer comprar? : " );
        dolar = sc.nextDouble();

        double totalDolar = CurrencyConverter.totalReais(dolar);
        System.out.println("Valor Total : "+ totalDolar);
    }
}
