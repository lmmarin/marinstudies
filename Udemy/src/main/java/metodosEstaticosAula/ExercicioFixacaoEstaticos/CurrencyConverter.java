package metodosEstaticosAula.ExercicioFixacaoEstaticos;

public class CurrencyConverter {

    private static final double PRICE_DOLAR = 3.10;
    private static final double IOF = 6.0;


    public static double totalReais(double x){

        x = x * PRICE_DOLAR; //VALOR ESPERADO X O PREÇO DO DOLAR 1 = 3.10
        x += (IOF/100) * x;  //Concatena com o que X já tem com a porcentagem que vai receber da conta.

        return x;
    }

}
