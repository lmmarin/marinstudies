package metodosEstaticosAula.exemplo1;

import java.util.Locale;
import java.util.Scanner;

    //Classe que trabalha com funcoes estaticas que so podem ser chamadas dentro de outros corpos staticos.

public class classEstaticExemple1 {

    private static final double PI = 3.1415;

    public static void main(String[] args) {

        double a;

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);

        System.out.println("Numero 1: ");
        a = sc.nextDouble();

        double c = circuference(a);
        System.out.println(c);
        double v = volume(a);
        System.out.println(v);

    }

    public static double circuference(double x){
      return 2.0 * PI * x;
    }

    public static double volume(double x){
        return 4.0 * PI * x * x * x / 3.0;
    }

}
