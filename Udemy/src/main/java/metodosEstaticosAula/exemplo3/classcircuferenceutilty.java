package metodosEstaticosAula.exemplo3;

public class classcircuferenceutilty {

    private static final double P_I = 3.1415;

    public static double circuference(double x){
        return 2.0 * P_I * x;
    }

    public static double volume(double x){
        return 4.0 * P_I * x * x * x / 3.0;
    }

}
