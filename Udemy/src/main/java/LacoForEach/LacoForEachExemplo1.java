package LacoForEach;

/**
 * @author lmarin
 * @version [: $<br/>
 * : $
 * @since 11/11/2019 16:17
 */
public class LacoForEachExemplo1 {

    public static void main(String[] args) {

        String[] arrString = new String[]{"Maria","Joaquin","Alex"};


        //nomeString.length a fun��o length quando � criado um array serve para ler ele inteiro.
        for(int i=0; i < arrString.length; i++){
            System.out.println(arrString[i]);
        }

        /*La�o for Each - ele da um apelido para cada valor que esta dentro do meu vetor no meu caso o obj
        Leitura : "Para cada objeto dentro do meu vetor, fa�a:"*/
        System.out.println("----------------------------------");
        for(String obj : arrString){
            System.out.println(obj);
        }


    }
}
