package exercicioFixacaoEncapsulamento;

public class contPerson {

    public int numberAccount;
    public String name;

    public contPerson(){
    }

    public contPerson(String nome,int NumeroDaConta){
        this.name = nome;
        this.numberAccount = NumeroDaConta;
    }

    public contPerson(String nome,int NumerodaConta,double saldodaconta){

        this.name = nome;
        this.saldo = saldodaconta;
        this.numberAccount = NumerodaConta;

    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    private double saldo;


    public String toString(){

        return " Nome: "+name+
                " | Numero da Conta: " +numberAccount+
                " | Saldo: "+saldo;
    }
}
