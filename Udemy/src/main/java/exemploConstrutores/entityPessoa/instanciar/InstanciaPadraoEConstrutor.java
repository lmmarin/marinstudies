package exemploConstrutores.entityPessoa.instanciar;

import exemploConstrutores.entityPessoa.PessoaEntity;

public class InstanciaPadraoEConstrutor {

    public static void main(String[] args) {

        //Pessoa inserida pelo metodo construtor, com a idade e o nome já colocado no momento de instanciar.
        PessoaEntity pessoaComValores = new PessoaEntity(10,"Juca");

        //Pessoa inserida sem o metodo construtor com valores, assim só podendo colocar valor a mão usando o sets!
        PessoaEntity pessoaEntity = new PessoaEntity();
        pessoaEntity.setIdade(1);
        pessoaEntity.setNome("Jose");
        pessoaEntity.setDetalhe("Gordo");

    }

}
