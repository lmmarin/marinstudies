package exemploConstrutores.entityPessoa;

public class PessoaEntity {

    public int idade;
    public String nome;
    public String detalhe;

    //Construtor sem Valor para instancia.
    public PessoaEntity(){
    }

    //Obriga a pessoa a colocar valores na hora de ser intanciado.
    public PessoaEntity(int idade,String nome){
        this.setIdade(idade);
        this.setNome(nome);
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDetalhe() {
        return detalhe;
    }

    public void setDetalhe(String detalhe) {
        this.detalhe = detalhe;
    }
}
