package ArraySintaxe.ExercicioFixacao.Entity;

public class Quarto {

    double valor;
    String nome,email;

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Quarto(double v,String n,String e){
        this.valor = v;
        this.nome = n;
        this.email = e;
    }

    public String toString(){
        return "Valor: "+valor+
                " Nome: "+nome+
                " E-mail: "+email;
    }

}
