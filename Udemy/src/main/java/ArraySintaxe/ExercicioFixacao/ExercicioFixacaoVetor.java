package ArraySintaxe.ExercicioFixacao;

import ArraySintaxe.ExercicioFixacao.Entity.Quarto;
import java.util.Locale;
import java.util.Scanner;


//Versão 1 não deixa que escolha o quarto que quer apenas a quantidade.
public class ExercicioFixacaoVetor {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner scanner = new Scanner(System.in);
        double preco;
        String nome,email;

        System.out.println("Nº de Quartos: ");
        int n = scanner.nextInt();
        Quarto[] VetorQuartos = new Quarto[n];

        for(int i = 1; i < n; i++){

            System.out.println("Valor Do Quarto P/ Dia: ");
            preco = scanner.nextDouble();

            System.out.println("Nome do Estudante: ");
            scanner.nextLine();
            nome = scanner.nextLine();

            System.out.println("E-mail: ");
            email = scanner.nextLine();

            VetorQuartos[i] = new Quarto(preco,nome,email);

        }

        for(int i=0; i < n; i++){
            System.out.println("Informações do Quarto: ["+i+"] "+VetorQuartos[i]);
        }


    }

}
