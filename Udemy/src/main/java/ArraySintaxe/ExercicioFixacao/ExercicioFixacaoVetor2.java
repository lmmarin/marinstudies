package ArraySintaxe.ExercicioFixacao;

import ArraySintaxe.ExercicioFixacao.Entity.Quarto;
import java.util.Locale;
import java.util.Scanner;

/**
 * @author lmarin
 * @version : $<br/>
 * : $
 * @since 11/11/2019 15:05
 */

//2 Vers�o a pessoa pode escolher o numero no vetor que quer ocupar e valida se est� em uso ou n�o
public class ExercicioFixacaoVetor2 {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner scanner = new Scanner(System.in);
        double preco;
        String nome,email;

        System.out.println("N� de Quartos: ");
        int n = scanner.nextInt();
        Quarto[] VetorQuartos = new Quarto[11];

        for(int i = 0; i < n; i++){

            System.out.println("Escolha o Quarto de 1 a 10: ");
            int q = scanner.nextInt();

            if(VetorQuartos[q]!= null){

                System.out.println("Quarto em Uso.");

            }else{

                System.out.println("Pre�o p/Dia: ");
                preco = scanner.nextDouble();

                System.out.println("Nome: ");
                scanner.nextLine();
                nome = scanner.nextLine();

                System.out.println("E-Mail: ");
                email = scanner.nextLine();

                VetorQuartos[q] = new Quarto(preco,nome,email);

            }


        }

        for(int i=0; i < VetorQuartos.length; i++){

            if(VetorQuartos[i]!=null)
            {
                System.out.println("Informa��es dos Quartos: ["+i+"] "+VetorQuartos[i]);
            }
        }

    }

}
