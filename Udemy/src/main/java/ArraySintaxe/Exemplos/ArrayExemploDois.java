package ArraySintaxe.Exemplos;

import ArraySintaxe.Exemplos.Entitys.Product;

import java.util.Locale;
import java.util.Scanner;

public class ArrayExemploDois {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner scanner = new Scanner(System.in);
        String nomeProduto;

        int n = 0;
        double preco=0;


        System.out.println("Quantos Produtos Para Cadastrar? ");
        n = scanner.nextInt();
        Product[] vetorProduto = new Product[n];

        for(int i=0; i<n; i++){

            System.out.println("Nome:");
            scanner.nextLine();
            nomeProduto = scanner.nextLine();

            System.out.println("Preco:");
            preco = scanner.nextDouble();

            vetorProduto[i] = new Product(nomeProduto, preco);
        }

        double somar = 0 ;

        for(int i=0; i<n; i++){
            somar += vetorProduto[i].getValor();
        }

        System.out.print("valor Total: "+somar);

    }
}
