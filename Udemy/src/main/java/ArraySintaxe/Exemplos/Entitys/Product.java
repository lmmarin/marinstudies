package ArraySintaxe.Exemplos.Entitys;

public class Product {

    String nome;
    double valor;

    public Product(){}

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Product(String n, Double v){
        this.nome = n;
        this.valor = v;
    }

}
