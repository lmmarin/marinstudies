**README MARIN**

Bem Vindo, para você que está vendo meu bitBucket ele é meu espaço de desenvolvimento que fico me entretendo quando tenho tempo! Aos poucos pode ir vendo exemplos de coisas que tenho feito e futuros projetos meus vão estar aqui também.


**Ferramentas Utilizadas**

-> vscode(Visual Microsoft Studio):

	- Para curso de JavaScript e de Golang é necessario ter ele com os plugins:
		-Code runner ( Para poder rodar seus codigos GO no proprio vscode ).
		-Dracula ( Deixa com um otimo visual para se trabalhar com a ferramenta)
		-Go ( Para poder suporte a linguagem do google ).
		-Material Icon Theme ( Ajuda a identificar o tipo de extensão que está trabalhando).

-> IntelliJ: 

	- IDE utilizada para trabalhar com Java(OO) com projeto Maven e Gradlew Futuramente.




**AGRADECIMENTOS**

Obrigado por visualizar e se caso quiser me ajudar com ideias pode me chamar pelo e-mail:
lucasmarinds@gmail.com


