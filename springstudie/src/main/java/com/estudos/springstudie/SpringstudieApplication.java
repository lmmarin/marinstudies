package com.estudos.springstudie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringstudieApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringstudieApplication.class, args);
	}

}
