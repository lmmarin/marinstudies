package com.estudos.springstudie.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lmarin
 * @version : $<br/>
 * : $
 * @since 11/10/2019 17:43
 */
@Controller
@RestController
public class controllerTest {

   @RequestMapping(value = "/",  produces = "application/json")
   @ResponseBody
   public String showMensage(){
       return "Hello World!";
   }


}
