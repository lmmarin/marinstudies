package main

import "testing"

func TestHello2(t *testing.T) {

	t.Run("Hello AA", func(t *testing.T) {
		got := HelloNothingConstant("AA")
		want := "Hello, AA"

		if got != want {
			t.Errorf("got '%s' want '%s'", got, want)
		}
	})

	t.Run("Hello World", func(t *testing.T) {
		got := HelloNothingConstant("world")
		want := "Hello, world"

		if got != want {
			t.Errorf("got '%s' want '%s'", got, want)
		}
	})

}
