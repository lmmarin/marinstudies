package main

import "testing"

/*A Classe de teste sempre deve haver no fim do nome dela *_test.go
Sempre deve haver como inicio da funcao de teste os caracteres Test
No argumento da função sempre deve haver (t *testing,T)
*/
func TestHello(t *testing.T) {
	got := HelloNothingConstant("Cat")
	want := "Hello, Cdat"

	//t.ErrorF > Mostra a mensagem se caso o teste falhar.
	if got != want {
		t.Errorf("got '%s' want '%s'", got, want)
	}
}

func TestHelloWithConstant(t *testing.T) {
	got := HelloWithConstant("Cat")
	want := "Hello, Cdat"

	if got != want {
		t.Errorf(got, want)
	}

}
