package main

//FMT é a classe que tem os recrusos de print e outros.
import "fmt"

const helloconstant = "Hello, "

func HelloNothingConstant(nome string) string {
	return "Hello, " + nome
}

func HelloWithConstant(nome string) string {
	return helloconstant + nome
}

//func Main > seria o metodo aonde vai se executar tudo que for colocado dentro dele.
func main() {
	fmt.Println(HelloNothingConstant("cat"))
}
