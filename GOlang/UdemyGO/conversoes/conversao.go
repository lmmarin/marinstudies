package main

import (
	"fmt"
	"strconv"
)

func main() {
	var x float64 = 2.30
	var y int64 = 3
	//Conversao simples de inteiro para float e float para inteiro
	fmt.Println(x / float64(y))
	fmt.Println(int64(x) / y)

	//Cuidado... se converter desta maneira vocÊ esta apontando para a tabela asc e pegando o valor não fazendo conversão
	fmt.Println("Conversando Int para String: ", string(123))

	//Maneira correta
	fmt.Println("Teste: ", strconv.Itoa(123))

	//String para Int e se caso passar um valor errado vai mostrar no erro o problema.
	num, error := strconv.Atoi("123")
	fmt.Println(num-122, error)

	//String to bool para não precisar estanciar a variavel error pode substituir com _
	b, _ := strconv.ParseBool("false")
	if b {
		fmt.Println("A Condição b é true? então mostra está mensagem.")
	} else {
		fmt.Println("Se não então mostre esta.")
	}

}
