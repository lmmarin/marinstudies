package main

import "fmt"

func main() {

	cont := 1

	for i := 0; i <= 20; i++ {

		if i%2 == 0 {
			fmt.Println("PAR:", i)
		}

	}
	fmt.Println("_____________________________________________________")
	for cont <= 25 {
		fmt.Println("Laço While: ", cont)
		cont++
	}
}
