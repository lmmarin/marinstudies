package main

import (
	"fmt"
	"math"
	"reflect"
)

func main() {
	//inteiros
	fmt.Println(1, 2, 1000)
	fmt.Println("Literal é do tipo:", reflect.TypeOf(10))

	//maximo valor de um inteiro
	i64 := math.MaxInt64
	fmt.Println("Biblioteca math mostra valor Maximo:", i64)

	//Valor de float64 que é o literal do float32
	var f64 float64 = 49.99
	fmt.Println(f64)

	//Valor Booleano
	bo := true
	fmt.Println("Valor Boolean é: ", bo)
	fmt.Println(!bo)
	fmt.Println("Tipo da variavel é: ", reflect.TypeOf(bo))

	//String
	s1 := "Oi Eu Sou o Lucas"
	fmt.Println(s1, "!")
	fmt.Println(len(s1))

	//String mostrando com quebra de linha a contagem total da variavel.
	s2 := `Oi
	Eu
	Sou
	O
	Lucas`
	
	sfunc main() {
		operacaoLogica := comprar(true, true)
		fmt.Println("TV50?'%t' TV40?'%t' Sorvete?'%t'", comprarTV50, comprarTV40, comprarNADA)
	}
	