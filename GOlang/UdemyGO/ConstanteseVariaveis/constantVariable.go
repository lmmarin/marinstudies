package main

import (
	"fmt"
	sysout "fmt"
	"math"
)

//Maneira alternativa de Criar Constantes.
const (
	a = 1
	b = 2.1
)

//Maneira alternativa de criar Variaveis.
var (
	c = 3.1
	d = 4
	e = b + c
)

func main() {
	z, x, y := 10, "SUCESSO!", 10.00
	//Constante e seu tipo;
	const PI float64 = 3.1415
	//Declarando Variavel e o proprio compilador já atribui o seu tipo padrão.
	var raio = 3.2
	// o ":=" significa que a variavel não existe(:) e já está sendo atribuido(=) um valor para ela e a forma mais usada.
	area := PI * math.Pow(raio, 2)

	fmt.Println("utilizando METODO do pacote")
	sysout.Println("Utilizando Metodo do pacote só que utilizando outro nome para o pacote.")
	sysout.Print("Area da Circuferencia: ", area)
	fmt.Println("a: '%d' b: '%f' c: '%f' d: '%d' e: '%f'", a, b, c, d, e)
	sysout.Printf("z:'%d' x:'%s' y:'%f'\n", z, x, y)

	//Fazendo conversão de Int ou Double para String
	real := 1.00
	conversao := fmt.Sprint(real)

	fmt.Println("Real:", conversao)

}
