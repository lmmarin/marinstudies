package main

import "fmt"

func somar(x float64, y float64) float64 {
	return x + y
}

func mostrarValor(resultado float64) {
	fmt.Println(resultado)
}
