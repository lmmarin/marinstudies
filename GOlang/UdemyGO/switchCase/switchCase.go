package main

import "fmt"

func switchCase(n float64) {
	var nota = int(n)
	switch nota {
	case 0, 1:
		fmt.Println("Precisa Estudar.")
	case 2:
		fallthrough
	case 3:
		fallthrough
	case 4:
		fmt.Println("Continue Tentando...")
	case 5, 6:
		fmt.Println("Foi bem")
	case 8:
		fmt.Println("Otimo.")
	default:
		fmt.Println("Nota Invalida")
	}
}

func switchCaseVersao2(i int) {
	idade := i
	switch {
	case idade <= 15:
		fmt.Println("Criança:")
	case idade <= 20:
		fmt.Println("Adoslecente")
	case idade > 20:
		fmt.Println("Adulto")
	}
}

func main() {
	switchCase(8)
	switchCase(2)
	switchCase(8.2)
	switchCase(15)
	fmt.Println("--------------------------------")
	switchCaseVersao2(12)
	switchCaseVersao2(18)
	switchCaseVersao2(21)
}
