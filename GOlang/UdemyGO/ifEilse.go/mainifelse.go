package main

import sysout "fmt"

func main() {

	sysout.Println("Teste de IF / Else:")
	mostrarSeAprovado(7, 7)
	mostrarSeAprovado(6, 7)
	mostrarSeAprovado(-10.0, 2.0)

}
