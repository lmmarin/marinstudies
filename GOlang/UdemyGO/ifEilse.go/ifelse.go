package main

import "fmt"

func mostrarSeAprovado(nota1, nota2 float64) {
	mediaFinal := (nota1 + nota2) / 2
	if mediaFinal >= 7 {
		fmt.Println("Foi Otimo com Media de: ", mediaFinal)
	} else if mediaFinal < 7 && mediaFinal > 0 {
		fmt.Println("Foi Normal Media: ", mediaFinal)
	} else {
		fmt.Println("Dados Informados Incorretos.")
	}
}
