package main

import "fmt"

func main() {
	var a int
	var b float64
	var c string
	var d bool
	var e *int

	//valores null em GO
	fmt.Printf("'%d' '%f' '%s'", a, b, c, d, e)
}
