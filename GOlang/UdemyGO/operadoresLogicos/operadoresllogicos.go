package main

func comprar(trabalho1, trabalho2 bool) (bool, bool, bool) {
	comprarTV50 := trabalho1 && trabalho2
	comprarTV40 := trabalho1 != trabalho2
	comprarNADA := trabalho1 || trabalho2

	return comprarTV50, comprarTV40, comprarNADA
}
