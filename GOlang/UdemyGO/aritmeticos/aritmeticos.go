package main

import (
	"fmt"
	"math"
)

func main() {
	a := 10
	b := 3

	fmt.Println("Somar:", a+b)
	fmt.Println("Subtração:", a-b)
	fmt.Println("Divisão:", a/b)
	fmt.Println("Multiplicação:", a*b)
	fmt.Println("Resto De Div:", a%b)

	//Math
	fmt.Println("Maior:", math.Max(float64(a), float64(b)))
	fmt.Println("Menor:", math.Min(float64(a), float64(b)))
	fmt.Println("Expotencial:", math.Pow(float64(a), float64(b))) //10 elevado a 3
}
