package main

import "fmt"

func main() {
	var i = 2

	var p *int = nil

	p = &i //pegando o endereço da variavel

	*p++ // desferenciando (pegar o valor)
	i++

	fmt.Println(p, *p, i, &i)
}
