package main

import "fmt"

func main() {
	var arrayTeste [10]float64 //Criando um Array no qual já define quantas posições vai ter  e o que vai receber.
	fmt.Println(arrayTeste)
	fmt.Println("------------------------------------------------------------------")

	ArrayRange := [...]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10} // Aqui o compilador conta quantos itens já existem e cria os espaços
	//var ArrayRange = [...]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10} Segunda Maneira de se criar um Array

	for _, numero := range ArrayRange {
		fmt.Print(numero, " ")
	}
	fmt.Println()
	fmt.Println("------------------------------------------------------------------")
	//Mostrando o Indice
	for i, numero := range ArrayRange {
		fmt.Printf(" [%d]=%d", i, numero)
	}
	fmt.Println()
	fmt.Println("------------------------------------------------------------------")
}
