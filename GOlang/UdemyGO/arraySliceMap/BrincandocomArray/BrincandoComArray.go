package main

import "fmt"

func main() {

	var arraySoma [2]float64

	arraySoma[0], arraySoma[1] = 7.5, 5.5

	var total float64

	for i := 0; i < len(arraySoma); i++ {
		total += arraySoma[i]
	}

	media := total / 2

	fmt.Println(media)

	fmt.Println("------------------------------------------------------------------------")

	var arrayParImparEAdicao [10]float64
	arrayParImparEAdicao[0], arrayParImparEAdicao[2], arrayParImparEAdicao[3], arrayParImparEAdicao[4] = 1.1, 2.2, 3.3, 4.4

	for i := 0; i < len(arrayParImparEAdicao); i++ {
		if arrayParImparEAdicao[i] == 0 {
			arrayParImparEAdicao[i] += 2
		} else {
			fmt.Println("Com Valor")
		}

	}

}
